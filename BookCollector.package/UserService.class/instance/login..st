as yet unclassified
login: anUser
	|tmpUser|

	anUser email ifEmpty: [ ^ CommonError withFailureMessage: (MessagesDecorator cannotBeNullOrEmpty: 'Email')  ].
	anUser password ifEmpty: [  ^ CommonError withFailureMessage: (MessagesDecorator cannotBeNullOrEmpty: 'Password') ].
	
	(users includesKey: anUser email) ifFalse: 
				[  ^ CommonError withFailureMessage: (MessagesDecorator doesNotExistWithObject: 'User' withProperty: 'username')].
	
	tmpUser := users at: (anUser email).
	
	((tmpUser password) = (anUser password))	
	ifTrue: [ 
			currentUser := tmpUser.
		 ^ CommonError withOkMessage: MessagesDecorator successfullyLogged
		 ].

	currentUser := nil.
	 ^ CommonError withFailureMessage: MessagesDecorator invalidPassword.