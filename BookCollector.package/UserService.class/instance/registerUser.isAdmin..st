as yet unclassified
registerUser: aUser isAdmin: adminRole
|newUser|
	aUser name ifEmpty: [ ^ CommonError withFailureMessage: (MessagesDecorator cannotBeNullOrEmpty: 'Name') ].
	aUser surname ifEmpty: [ ^ CommonError withFailureMessage: (MessagesDecorator cannotBeNullOrEmpty: 'Surname')  ].
	aUser email ifEmpty: [ ^ CommonError withFailureMessage: (MessagesDecorator cannotBeNullOrEmpty: 'Email') ].
	aUser password ifEmpty: [ ^ CommonError withFailureMessage: (MessagesDecorator cannotBeNullOrEmpty: 'Password') ].
	(users includesKey: aUser email)
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesDecorator alreadyExistsWithObject: 'User' withProperty: 'email') ].
		
	adminRole ifTrue: [newUser:=Admin new  ]   ifFalse: [newUser:=Customer new].	
	newUser name: aUser name.
	newUser surname: aUser surname.
	newUser email: aUser email.
	newUser password: aUser password.
	
	self register: newUser.
	DatabaseSupport save.
	^ CommonError withOkMessage: MessagesDecorator successfullyDone.