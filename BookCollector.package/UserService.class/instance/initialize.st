initializing
initialize
	|admin user|
	
	super initialize.
	users := Dictionary new.
	
	admin := Admin new.
	admin name: 'Admin'.
	admin surname: 'Adminovski'.
	admin email: 'admin'.
	admin password: 'admin'.
	admin borrowedCopies: Dictionary new.
	
	user := Customer new.
	user name: 'User'.
	user surname: 'Userovski'.
	user email: 'user'.
	user password: 'user'.
	user borrowedCopies: Dictionary new.
	
	users at: 'admin' put: admin.
	users at: 'user' put: user.
	
