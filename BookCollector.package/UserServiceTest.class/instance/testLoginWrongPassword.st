tests
testLoginWrongPassword
 |customer logingCustomer result|
	
	TestSupport prepareForTest.
	
	customer:=Customer new.
	customer name: 'name'.
	customer surname: 'surname'.
	customer email: 'email'.
	customer password: 'password'.
	UserService instance registerUser: customer isAdmin: false.
	
	logingCustomer := User new.
	logingCustomer email: customer email.
	logingCustomer password: 'wrong password'.

	result :=UserService instance login: logingCustomer.

	self assert: result isOk not.
				self assert: result message equals: MessagesDecorator invalidPassword.



