tests
testRegister
 |size user instance|

TestSupport prepareForTest.

user:=User new.
user name: 'name'.
instance:= UserService instance.

size:= instance users size.
instance register: user.

self assert: instance users size = (size+1).


