tests
testLogin
 |admin logingAdmin  result service|
	
	TestSupport prepareForTest.
	
	service := UserService instance.
	
	admin:=Admin new.
	admin name: 'name'.
	admin surname: 'surname'.
	admin email: 'email'.
	admin password: 'password'.
	service registerUser: admin isAdmin: true.
	
	logingAdmin := User new.
	logingAdmin email: admin email.
	logingAdmin password: admin password.
	

	result :=service login: logingAdmin.

	self assert: result isOk.
				self assert: result message equals: MessagesDecorator successfullyLogged.



