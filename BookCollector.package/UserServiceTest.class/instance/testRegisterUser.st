tests
testRegisterUser
 |size admin instance result|

TestSupport prepareForTest.

	instance:= UserService instance.

	admin:=User new.
	admin name: 'name'.
	admin surname: 'surname'.
	admin email: 'email'.
	admin password: 'password'.
	 
	size:= instance users size.

	result :=instance registerUser: admin isAdmin: true.

	self assert: result isOk.
	self assert: result message equals: MessagesDecorator successfullyDone.
	self assert: instance users size = (size+1).


