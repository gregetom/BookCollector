tests
testRegisterPublisher
	|publisher service size|
	
	TestSupport prepareForTest.
	
	service:=PublisherService instance.
	
	publisher := Publisher new.
	publisher name: 'name'.

	
	size:=service publishers size.
	
	service register: publisher.
	
	self assert: service publishers size = (size+1).
