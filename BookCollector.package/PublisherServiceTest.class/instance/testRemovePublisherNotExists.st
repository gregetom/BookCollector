tests
testRemovePublisherNotExists
	|publisher result|
	
	publisher := Publisher new.
	publisher name: 'name'.
	
	result := PublisherService instance removePublisher: publisher.
	
	self assert: result isOk not.
				self assert: result message equals: (MessagesDecorator doesNotExistWithObject: 'Publisher'  withProperty:  'name' ).