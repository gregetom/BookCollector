tests
testRemovePublisher
	|publisher result|
	
	publisher := Publisher new.
	publisher name: 'name'.
	result := PublisherService instance addPublisher: publisher.
	
	self assert: PublisherService instance publishers size > 0.
	
	result := PublisherService instance removePublisher: publisher.
	
	self assert: result isOk.
				self assert: result message equals: MessagesDecorator successfullyDone.