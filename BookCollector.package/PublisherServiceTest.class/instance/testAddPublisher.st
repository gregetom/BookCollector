tests
testAddPublisher
	|result publisher|
	
	TestSupport prepareForTest.
	
	publisher := Publisher new.
	publisher name: 'name'.
	
	result := PublisherService instance addPublisher: publisher.
	
	self assert: result isOk.
				self assert: result message equals: MessagesDecorator successfullyDone.