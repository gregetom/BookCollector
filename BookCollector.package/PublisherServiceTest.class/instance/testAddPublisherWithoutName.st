tests
testAddPublisherWithoutName
	|result publisher|
	
	publisher := Publisher new.
	publisher name: ''.
	
	result := PublisherService instance addPublisher: publisher.
	
	self assert: result isOk not.
				self assert: result message equals: (MessagesDecorator cannotBeNullOrEmpty: 'Name' ).
