as yet unclassified
returnBook: aCopyNumber
	| userService copy |
	userService := UserService instance.
	
	aCopyNumber ifEmpty: [ ^ CommonError withFailureMessage: (MessagesDecorator cannotBeNullOrEmpty: 'Copy number') ].
	copy := self findCopy: aCopyNumber.
	copy ifNil: [ ^ CommonError withFailureMessage: (MessagesDecorator doesNotExistWithObject: 'Copy' withProperty:  'number')].
	
	copy borrowed: false.
	userService currentUser borrowedCopies removeKey: aCopyNumber.
	DatabaseSupport save.
	^ CommonError withOkMessage: MessagesDecorator successfullyDone.