private
createBook: aBook

	aBook bookNumber isNil | aBook bookNumber isEmpty
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesDecorator cannotBeNullOrEmpty: 'Book number') ].
	aBook title isNil | aBook title isEmpty
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesDecorator cannotBeNullOrEmpty: 'Title') ].
	aBook author isNil | aBook author isEmpty
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesDecorator cannotBeNullOrEmpty: 'Author') ].
	aBook genre isNil | aBook genre isEmpty
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesDecorator cannotBeNullOrEmpty: 'Genre') ].
	aBook publisher ifNil: [ ^ CommonError withFailureMessage: (MessagesDecorator cannotBeNullOrEmpty: 'Publisher') ].
	(books includesKey: aBook bookNumber)
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesDecorator alreadyExistsWithObject: 'Book' withProperty: 'number') ].
	self register: aBook.
	DatabaseSupport save.
	^ CommonError withOkMessage: MessagesDecorator successfullyDone.