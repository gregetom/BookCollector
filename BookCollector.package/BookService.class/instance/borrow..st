as yet unclassified
borrow: aBookNumber
	| availableCopyList borrowedCopyNumber userService |
	userService := UserService instance.
	(books includesKey: aBookNumber)
		ifFalse: [ ^ CommonError withFailureMessage: 'Book with this number does not exists' ].
	availableCopyList := (books at: aBookNumber) availableCopies.
	availableCopyList
		ifEmpty: [ ^ CommonError withFailureMessage: 'Sorry, no available copy of this book' ].
	borrowedCopyNumber := (availableCopyList at: 1) copyNumber.
	(availableCopyList at: 1) borrowed: true.
	userService currentUser borrowedCopies
		at: borrowedCopyNumber
		put: (availableCopyList at: 1).
		DatabaseSupport save.
	^ CommonError withOkMessage: 'Your number number is: ' , borrowedCopyNumber