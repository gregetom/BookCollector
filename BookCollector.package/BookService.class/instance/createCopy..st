private
createCopy: aCopy
	| book copy |
	aCopy copyNumber isEmpty
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesDecorator cannotBeNullOrEmpty: 'Copy number') ].
	(books includesKey: aCopy bookNumber)
		ifFalse: [ ^ CommonError withFailureMessage: (MessagesDecorator mustExistsWithObject: 'Book' withProperty: 'book number') ].
	copy := self findCopy: aCopy copyNumber.
	copy ifNotNil: [ ^ CommonError withFailureMessage: (MessagesDecorator alreadyExistsWithObject: 'Copy' withProperty:  'copy number') ].
	book := books at: aCopy bookNumber.
	book copyList at: aCopy copyNumber put: aCopy.

	DatabaseSupport save.
	^ CommonError withOkMessage: MessagesDecorator successfullyDone.