as yet unclassified
findCopy: aCopyNumber
	|bookKeys copyKeys copies|
	
	bookKeys := books keys.
	bookKeys do: [ :bk |
		copies := (books at: bk) copyList.
		copyKeys := copies keys.
		
		copyKeys do: [ :ck |
				(copies at: ck) copyNumber = aCopyNumber ifTrue: [^ copies at: ck ].
			 ]. 
		 
 	].

	^ nil