tests
testCreateBookWithoutBookNumber
	|book result|
	book := Book new.
	
	book bookNumber: ''.
	book title: 'title'.
	book author: 'author'.
	book genre: 'genre'.
	book publisher: Publisher new.	
	
	
	result := BookService instance createBook: book.
	
	self assert: result isOk not.
			self assert: result message equals: (MessagesDecorator cannotBeNullOrEmpty: 'Book number' ).