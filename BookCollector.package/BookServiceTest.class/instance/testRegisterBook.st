tests
testRegisterBook
	|book service size|
	
	TestSupport prepareForTest.
	
	service:=BookService instance.
	
	book := Book new.
	book bookNumber: '123'.
	book title: 'title'.
	book author: 'author'.
	book genre: 'genre'.	
	
	size:=service books size.
	
	service register: book.
	
	self assert: service books size = (size+1).