tests
testCreateCopyWithoutBookNumber
	|book copy result|
	
	book := Book new.
	book bookNumber: '123'.
	book title: 'title'.
	book author: 'author'.
	book genre: 'genre'.
	book publisher: Publisher new.	
	BookService instance createBook: book.
	
	copy:=BookCopy new.
	copy bookNumber: ''.
	copy copyNumber: '456'.
	 
	result := BookService instance createCopy: copy.
	
	self assert: result isOk not.
			self assert: result message equals: (MessagesDecorator mustExistsWithObject: 'Book' withProperty:  'book number' ).