tests
testRemoveNotExists
	|result|
	
	result := BookService instance remove: '123'. 
		
	self assert: result isOk not.
			self assert: result message equals: (MessagesDecorator doesNotExistWithObject: 'Book' withProperty:  'number' ).