tests
testRemove
	|book result|
	
	TestSupport prepareForTest.
	
	book := Book new.
	book bookNumber: '123'.
	book title: 'title'.
	book author: 'author'.
	book genre: 'genre'.
	book publisher: Publisher new.	
	BookService instance createBook: book.
	
	result := BookService instance remove: book bookNumber. 
		
	self assert: result isOk.
			self assert: result message equals: MessagesDecorator successfullyDone.