tests
testFindCopyNotFound
	|result|
	
	result := BookService instance findCopy: '123'. 
		
	self assert: result isNil.