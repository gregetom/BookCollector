tests
testCreateCopyWithoutCopyNumber
	|book copy result|
	
	book := Book new.
	book bookNumber: '123'.
	book title: 'title'.
	book author: 'author'.
	book genre: 'genre'.
	book publisher: Publisher new.	
	BookService instance createBook: book.
	
	copy:=BookCopy new.
	copy bookNumber: book bookNumber..
	copy copyNumber: ''.
	 
	result := BookService instance createCopy: copy.
	
	self assert: result isOk not.
			self assert: result message equals: (MessagesDecorator cannotBeNullOrEmpty: 'Copy number' ).