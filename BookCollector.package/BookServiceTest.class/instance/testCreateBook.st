tests
testCreateBook
	|book result|
	
	TestSupport prepareForTest.
	
	book := Book new.
	
	book bookNumber: '123'.
	book title: 'title'.
	book author: 'author'.
	book genre: 'genre'.
	book publisher: Publisher new.	
	
	
	result := BookService instance createBook: book.
	
	self assert: result isOk.
	self assert: result message equals: MessagesDecorator successfullyDone.