tests
testCreateCopy
	|book copy result|
	
	TestSupport prepareForTest.
	
	book := Book new.
	book bookNumber: '123'.
	book title: 'title'.
	book author: 'author'.
	book genre: 'genre'.
	book publisher: Publisher new.	
	BookService instance createBook: book.
	
	copy:=BookCopy new.
	copy bookNumber: book bookNumber.
	copy copyNumber: '456'.
	 
	result := BookService instance createCopy: copy.
	
	self assert: result isOk.
			self assert: result message equals: MessagesDecorator successfullyDone.