adding
addPublisher: aPublisher
	aPublisher name ifEmpty: [ ^ CommonError withFailureMessage:(MessagesDecorator cannotBeNullOrEmpty: 'Name') ].
	(publishers includesKey: aPublisher name)
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesDecorator alreadyExistsWithObject: 'Publisher' withProperty:'name' ) ].
	self register: aPublisher.
	DatabaseSupport save.
	^ CommonError withOkMessage: MessagesDecorator successfullyDone.