removing
removePublisher: aPublisher
	| currentUser |
	currentUser := UserService instance currentUser.

	(publishers includesKey: aPublisher name)
		ifFalse: [ ^ CommonError withFailureMessage: (MessagesDecorator doesNotExistWithObject: 'Publisher' withProperty: 'name' ) ].

	publishers removeKey: aPublisher name.
	DatabaseSupport save.
	^ CommonError withOkMessage: MessagesDecorator successfullyDone.