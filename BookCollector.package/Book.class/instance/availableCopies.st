as yet unclassified
availableCopies
	|keys copies|
	
	copies := OrderedCollection new.
	keys := self copyList keys.
	keys do: [ :each | (copyList at: each) borrowed ifFalse: [ copies add: (copyList at: each) ]. ].
	
	^ copies.