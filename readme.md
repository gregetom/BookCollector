<strong>16/11/2017</strong>
<ul>
<li>Project creation and research of needed frameworks and technologies.Set up of repository and initial set up of the project.</li>
<li>implementation of project basics and the general gist.</li>
</ul>
<strong>24/11/2017</strong>
<ul>
<li>Basic work schedule created and implementation of basic web UI.Seaside server basics built and the prototype of web tested.Book classes created and added functionality of book copies.
Data validation on current system and creation of a singleton for booklist collection.</li>
<li>Implementation of UI and internal class structure.</li>
</ul>
<strong>1/12/2017</strong>
<ul>
<li>implemented web ui , added functionality login for dummy users for now, added more ui elements. Book copy functionality added giving the ability for a user to reserve a book.
Added roles for Users - User, admin. Added functional elements for manipulation with book coppies.</li>
<li>Continued work on the internal class structure, adding functionality to the web and support for database manipulation (STON/NeoJSTON) and increasing test coverage.</li>
</ul>
<strong>7/12/2017</strong>
<ul>
<li>Web UI completion, internal class structure created. UI functionality added for data manipulation ie. adding, removing book copies. Research of STON/neoJSTON and login seassion for users logged in on site alowing for more people to be logged at the same time. Adding test coverage , cause why not.</li>
<li>Database manipulation, test coverage (thats good). </li>
</ul>
<strong>15/12/2017</strong>
<ul>
<li>Added save and load functionality to classes and a DatabaseSupport class that uses methods - neoJsonOn: to save and load (map) from and to json file, refactoring of internals, change of singletons to accomodate for load and save.</li>
<li>Adding visitor pattern, test coverage (current around 50%), Documentation and install guide</li>
</ul>
<strong>22/12/2017</strong>
<ul>
<li>Added visitor pattern to user -> admin , user , polymorfism , changed load and save to accomodate the changes to classes and did some refactoring, completed install guide and documentation, test coverage 80% :)</li>
<li>finishing the project , done</li>
</ul>