loading
readLines: aStream
|line|
[ (line := aStream nextLine) isNil ]
		whileFalse: [ line = '#book'
				ifTrue: [ line := aStream nextLine.
					self loadBookFromJson: line ]
				ifFalse: [ line = '#publisher'
						ifTrue: [ line := aStream nextLine.
							self loadPublisherFromJson: line ]
						ifFalse: [ line = '#user'
								ifTrue: [ line := aStream nextLine.
									self loadUserFromJson: line ]
								ifFalse: [ line = '#admin'
										ifTrue: [ line := aStream nextLine.
											self loadAdminFromJson: line ]
										ifFalse: [ line = '#customer'
												ifTrue: [ line := aStream nextLine.
													self loadCustomerFromJson: line ] ] ] ] ] ].