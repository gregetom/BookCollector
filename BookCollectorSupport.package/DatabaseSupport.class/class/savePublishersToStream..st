actions
savePublishersToStream: aStream
	PublisherService instance publishers
		do: [ :each | 
			aStream nextPutAll: '#publisher'.
			aStream nextPut: Character cr.
			aStream nextPutAll: (NeoJSONWriter toString: each).
			aStream nextPut: Character cr ]