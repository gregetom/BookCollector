loading
mapNeoJsonOnBookCopy: neoJsonMapper for: aClass
	neoJsonMapper
		for: aClass
		do: [ :mapping | mapping mapInstVars: #(bookNumber copyNumber borrowed) ]