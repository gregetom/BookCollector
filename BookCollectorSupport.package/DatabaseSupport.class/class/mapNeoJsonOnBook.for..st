loading
mapNeoJsonOnBook: neoJsonMapper for: aClass
	self mapNeoJsonOnBookCopy: neoJsonMapper  for: BookCopy. 
	neoJsonMapper
		for: aClass
			do: [ :mapping | 
			mapping mapInstVars: #(author title bookNumber copyList).
			(mapping mapInstVar: #copyList) valueSchema: #BookCopyMap.
			mapping mapInstVars: #(genre publisher) ];
		for: #BookCopyMap customDo: [ :mapping | mapping mapWithValueSchema: BookCopy ].