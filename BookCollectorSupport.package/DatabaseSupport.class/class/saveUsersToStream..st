actions
saveUsersToStream: aStream
	UserService instance users
		do: [ :each | 
			each class = Admin
				ifTrue: [ aStream nextPutAll: '#admin' ]
				ifFalse: [ aStream nextPutAll: '#customer' ].
			aStream nextPut: Character cr.
			aStream nextPutAll: (NeoJSONWriter toString: each).
			aStream nextPut: Character cr ]