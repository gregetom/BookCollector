actions
loadBookFromJson: aString
	BookService instance 
		register:
			((NeoJSONReader on: aString readStream)
				for: BookCopy
					do: [ :mapping | mapping mapInstVars: #(bookNumber copyNumber borrowed) ];
				for: Publisher do: [ :mapping | mapping mapInstVar: #name ];
				for: #BookCopyMap customDo: [ :mapping | mapping mapWithValueSchema: BookCopy ];
				mapInstVarsFor: Book;
				for: Book
					do: [ :mapping | 
					(mapping mapInstVar: #publisher) valueSchema: Publisher.
					(mapping mapInstVar: #copyList) valueSchema: #BookCopyMap ];
				nextAs: Book)