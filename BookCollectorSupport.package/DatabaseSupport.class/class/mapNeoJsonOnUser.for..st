loading
mapNeoJsonOnUser: neoJsonMapper for: aClass
	neoJsonMapper
		for: #BookCopyMap customDo: [ :mapping | mapping mapWithValueSchema: BookCopy ];
		for: aClass
			do: [ :mapping | 
			mapping mapInstVars: #(name surname email password).
			(mapping mapInstVar: #borrowedCopies) valueSchema: #BookCopyMap ].