actions
saveBooksToStream: aStream
	BookService instance books
		do: [ :each | 
			aStream nextPutAll: '#book'.
			aStream nextPut: Character cr.
			aStream nextPutAll: (NeoJSONWriter toString: each).
			aStream nextPut: Character cr ]