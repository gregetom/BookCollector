loading
load
	| stream dir |
	dir := FileSystem disk workingDirectory.
	stream := (dir / 'saveFile') readStream.
	BookService instance removeAllBooks.
	PublisherService instance removeAllPublishers.
	UserService instance removeAllUsers.
	self readLines: stream.
	stream close