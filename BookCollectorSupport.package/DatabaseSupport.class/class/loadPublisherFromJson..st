actions
loadPublisherFromJson: aString
	PublisherService instance
		register:
			((NeoJSONReader on: aString readStream)
				mapInstVarsFor: Publisher;
				nextAs: Publisher)