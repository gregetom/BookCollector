actions
loadUserFromJson: aString
	^ (NeoJSONReader on: aString readStream)
		mapInstVarsFor: User;
		for: BookCopy do: [ :mapping | mapping mapInstVars: #(bookNumber copyNumber borrowed) ];
		for: #BookCopyMap customDo: [ :mapping | mapping mapWithValueSchema: BookCopy ];
		mapInstVarsFor: Book;
		for: User
			do: [ :mapping | (mapping mapInstVar: #borrowedCopies) valueSchema: #BookCopyMap ];
		nextAs: User