actions
save
	| stream dir |
	dir := FileSystem disk workingDirectory.
	(dir / 'saveFile') asFileReference exists
		ifTrue: [ (dir / 'saveFile') delete ].
	stream := (dir / 'saveFile') writeStream.
	self saveBooksToStream: stream.
	self savePublishersToStream: stream.
	self saveUsersToStream: stream.
	stream close