visiting
userReport
	|users keys|
	
	users := UserService instance users.
	
	keys := users keys.
	keys do: [ :each | (users at: each) accept: self ].

	^ self report