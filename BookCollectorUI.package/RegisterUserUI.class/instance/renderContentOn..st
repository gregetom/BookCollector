rendering
renderContentOn: html
	| adminRole newUser currentUser |
	newUser := User new.
	adminRole:=false.
	currentUser:= UserService instance currentUser.
	MainWindowUI new renderHeader: html.
	html div
		class: 'container';
		with: [ html div
				class: 'container-fluid text-center';
				style: 'margin-top:12rem';
				with: [ html div
						class: 'col-sm-2';
						with: [  ].
					html div
						class: 'col-sm-8';
						with: [ html div
								class: 'well';
								with: [ html heading
										level: 2;
										with: [ html strong: 'Please fill in the boxes' ].
									registerErrorContainer successMessage
										ifNotNil: [ html div
												class: 'alert alert-success';
												with: registerErrorContainer successMessage ].
									registerErrorContainer failureMessage
										ifNotNil: [ html div
												class: 'alert alert-danger';
												with: registerErrorContainer failureMessage ].
									html
										form: [ html label: 'Name'.
											html textInput
												class: 'form-control';
												callback: [ :value | newUser name: value ].
											html break.
											html label: 'Surname'.
											html textInput
												class: 'form-control';
												callback: [ :value | newUser surname: value ].
											html break.
											html label: 'Email'.
											html textInput
												class: 'form-control';
												callback: [ :value | newUser email: value ].
											html break.
											html label: 'Password'.
											html passwordInput 
												class: 'form-control';
												callback: [ :value | newUser password: value ].
											html break.
											
											currentUser isAdmin ifTrue: [ 
											html label: 'Admin or User'.
											html checkbox
												class: 'form-control';
												callback: [ :value | adminRole := value ].
											html break.
											].
											html button
												class: 'btn btn-primary btn-block center';
												callback: [ self registerUserUI: newUser isAdmin: adminRole ];
												with: 'Register' ] ] ] ] ]