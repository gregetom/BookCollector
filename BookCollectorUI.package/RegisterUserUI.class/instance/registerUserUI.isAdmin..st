action
registerUserUI: anUser isAdmin: adminRole
	|commonError|
	
	commonError := UserService instance registerUser: anUser isAdmin: adminRole.
	
		registerErrorContainer := CommonErrorContainer with: commonError.
	
			