as yet unclassified
returnBookUI: aCopyNumber
	|commonError|
	
	commonError := BookService instance returnBook: aCopyNumber.
	
	returnBookErrorContainer := CommonErrorContainer with: commonError.