initialization
initialize
	|visitor|
	
	super initialize.
	
	visitor := UserVisitor new.
	report := visitor userReport.