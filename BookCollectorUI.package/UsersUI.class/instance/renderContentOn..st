rendering
renderContentOn: html
	MainWindowUI new renderHeader: html.
	html div
		class: 'container';
		with: [ html div
				class: 'container-fluid';
				style: 'margin-top:12rem';
				with: [ html div
						class: 'col-sm-12 well';
						with: [ html heading
								class: 'text-center';
								level: 2;
								with: [ html strong: 'Users' ].
							html horizontalRule.
							report
								do: [ :each | 
									html text: each.
									html break ] ] ] ]