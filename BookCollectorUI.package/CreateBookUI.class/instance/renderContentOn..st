rendering
renderContentOn: html
	| newBook |
	newBook := Book new.
	MainWindowUI new renderHeader: html.
	html div
		class: 'container';
		with: [ html div
				class: 'container-fluid text-center';
				style: 'margin-top:12rem';
				with: [ html div
						class: 'col-sm-12';
						with: [ html div
								class: 'col-sm-6';
								with: [ self renderCreateBook: html ].
							html div
								class: 'col-sm-6';
								with: [ self renderCreateCopy: html ] ] ] ]