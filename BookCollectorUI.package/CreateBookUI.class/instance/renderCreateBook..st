rendering
renderCreateBook: html
	| newBook publishers |
	newBook := Book new.
	publishers := PublisherService instance publishers.
	html div
		class: 'col-sm-12';
		with: [ html div
				class: 'well';
				with: [ html heading
						level: 2;
						with: [ html strong: 'New book' ].
					createBookErrorContainer successMessage
						ifNotNil: [ html div
								class: 'alert alert-success';
								with: createBookErrorContainer successMessage ].
					createBookErrorContainer failureMessage
						ifNotNil: [ html div
								class: 'alert alert-danger';
								with: createBookErrorContainer failureMessage ].
					html
						form: [ html label: 'Book number (id)'.
							html textInput
								class: 'form-control';
								callback: [ :value | newBook bookNumber: value ].
							html break.
							html label: 'Title'.
							html textInput
								class: 'form-control';
								callback: [ :value | newBook title: value ].
							html break.
							html label: 'Author'.
							html textInput
								class: 'form-control';
								callback: [ :value | newBook author: value ].
							html break.
							html label: 'Genre'.
							html textInput
								class: 'form-control';
								callback: [ :value | newBook genre: value ].
							html break.
							html label: 'Publisher'.
							html select
								class: 'form-control';
								list: publishers;
								labels: [ :ea | ea name ];
								callback: [ :value | newBook publisher: value ].
							html break.
							html button
								class: 'btn btn-primary btn-block center';
								callback: [ self createBookUI: newBook ];
								with: 'Create' ] ] ]