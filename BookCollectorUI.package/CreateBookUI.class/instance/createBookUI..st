action
createBookUI: aBook
	|commonError|
	
	commonError :=BookService instance createBook: aBook.

	createBookErrorContainer := CommonErrorContainer with: commonError.
	
	