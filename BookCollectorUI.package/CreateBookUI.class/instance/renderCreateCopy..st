rendering
renderCreateCopy: html
	| newCopy |
	newCopy := BookCopy new.
	bookNumbers
		ifNotNil: [ html div
				class: 'col-sm-12';
				with: [ html div
						class: 'well';
						with: [ html heading
								level: 2;
								with: [ html strong: 'New copy' ].
							createCopyErrorContainer successMessage
								ifNotNil: [ html div
										class: 'alert alert-success';
										with: createCopyErrorContainer successMessage ].
							createCopyErrorContainer failureMessage
								ifNotNil: [ html div
										class: 'alert alert-danger';
										with: createCopyErrorContainer failureMessage ].
							html
								form: [ html label: 'Copy number (id)'.
									html textInput
										class: 'form-control';
										callback: [ :value | newCopy copyNumber: value ].
									html break.
									html label: 'Book'.
									html select
										class: 'form-control';
										list: bookNumbers;
										labels: [ :ea | ea bookNumber , ' ' , ea title ];
										callback: [ :value | newCopy bookNumber: value bookNumber ].
									html break.
									html button
										class: 'btn btn-primary btn-block center';
										callback: [ self createCopyUI: newCopy ];
										with: 'Create' ] ] ] ]