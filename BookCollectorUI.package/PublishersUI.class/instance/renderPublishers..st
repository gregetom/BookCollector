rendering
renderPublishers: html
	| publishers currentUser |
	currentUser := UserService instance currentUser.
	publishers := PublisherService instance publishers.
	html div
		class: 'col-sm-12 well';
		with: [ html heading
				level: 2;
				with: [ html strong: 'Publishers' ].
			removePublisherErrorContainer successMessage
				ifNotNil: [ html div
						class: 'alert alert-success';
						with: removePublisherErrorContainer successMessage ].
			removePublisherErrorContainer failureMessage
				ifNotNil: [ html div
						class: 'alert alert-danger';
						with: removePublisherErrorContainer failureMessage ].
			html div
				class: 'col-sm-6';
				with: [ html heading
						level: 4;
						with: [ html strong: 'Name' ] ].
			currentUser isAdmin
						ifTrue: [ html div
								class: 'col-sm-6';
								with: [ html heading
										level: 4;
										with: [ html strong: 'Remove' ] ] ].
			html break.
			html horizontalRule.
			publishers isEmpty
				ifTrue: [ html div
						class: 'col-sm-12 alert alert-warning';
						with: [ html text: 'No available publishers' ] ].
			publishers
				do: [ :each | 
					html div
						class: 'col-sm-6';
						with: [ html text: each name ].
					currentUser
						ifNotNil: [ currentUser isAdmin
								ifTrue: [ html div
										class: 'col-sm-6';
										with: [ html
												form: [ html button
														class: 'glyphicon glyphicon-remove btn btn-danger';
														callback: [ self removePublisherUI: each ] ] ] ] ].
					html break.
					html break.
					html break ] ]