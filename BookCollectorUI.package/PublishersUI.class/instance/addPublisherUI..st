action
addPublisherUI: aPublisher
	| commonError|
	
	commonError := PublisherService instance addPublisher: aPublisher.
	
	addPublisherErrorContainer := CommonErrorContainer with: commonError.
