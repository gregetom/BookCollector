rendering
renderAddPublisher: html
	| publisher |
	publisher := Publisher new.
	html div
		class: 'col-sm-12 well';
		with: [ html heading
				level: 2;
				with: [ html strong: 'New publisher' ].
			addPublisherErrorContainer successMessage
				ifNotNil: [ html div
						class: 'alert alert-success';
						with: addPublisherErrorContainer successMessage ].
			addPublisherErrorContainer failureMessage
				ifNotNil: [ html div
						class: 'alert alert-danger';
						with: addPublisherErrorContainer failureMessage ].
			html
				form: [ html label: 'Name'.
					html textInput
						class: 'form-control';
						callback: [ :value | publisher name: value ].
					html break.
					html button
						class: 'btn btn-primary btn-block center';
						callback: [ self addPublisherUI: publisher ];
						with: 'Create' ] ]