removing
removeUI: aBookNumber
	|commonError|
	
	commonError := BookService instance remove: aBookNumber.
	
	booksErrorContainer  := CommonErrorContainer with: commonError.