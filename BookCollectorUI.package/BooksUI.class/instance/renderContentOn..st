rendering
renderContentOn: html
	| currentUser bookDetail |
	currentUser := UserService instance currentUser.
	bookDetail := ObjectDetailSupport instance book.
	MainWindowUI new renderHeader: html.
	bookDetail ifNotNil: [ self renderDetail: html ].
	html div
		class: 'container';
		with: [ html div
				class: 'container-fluid text-center';
				style: 'margin-top:12rem';
				with: [ html div
						class: 'col-sm-12 well';
						with: [ html heading
								level: 2;
								with: [ html strong: 'Books' ].
							booksErrorContainer successMessage
								ifNotNil: [ html div
										class: 'alert alert-success';
										with: booksErrorContainer successMessage ].
							booksErrorContainer failureMessage
								ifNotNil: [ html div
										class: 'alert alert-danger';
										with: booksErrorContainer failureMessage ].
							html div
								class: 'col-sm-3';
								with: [ html heading
										level: 4;
										with: [ html strong: 'Title' ] ].
							html div
								class: 'col-sm-3';
								with: [ html heading
										level: 4;
										with: [ html strong: 'Author' ] ].
							html div
								class: 'col-sm-3';
								with: [ html heading
										level: 4;
										with: [ html strong: 'Free / Total' ] ].
							currentUser isNotNil ifTrue: [  		
							html div
								class: 'col-sm-1';
								with: [ html heading
										level: 4;
										with: [ html strong: 'Borrow' ] ].
							].
							html div
								class: 'col-sm-1';
								with: [ html heading
										level: 4;
										with: [ html strong: 'Detail' ] ].
							currentUser isAdmin
										ifTrue: [ html div
												class: 'col-sm-1';
												with: [ html heading
														level: 4;
														with: [ html strong: 'Remove' ] ] ].
							html break.
							html horizontalRule.
							books isEmpty
								ifTrue: [ html div
										class: 'col-sm-12 alert alert-warning';
										with: [ html text: 'Sorry, no available books' ] ].
							books
								do: [ :each | 
									html div
										class: 'col-sm-3';
										with: [ html text: each title ].
									html div
										class: 'col-sm-3';
										with: [ html text: each author ].
									html div
										class: 'col-sm-3';
										with: [ html text: each availableCopies size.
											html text: ' / '.
											html text: each copyList size ].
									currentUser isNotNil ifTrue: [  		
									html div
										class: 'col-sm-1';
										with: [ html
												form: [ html button
														class: 'btn btn-primary glyphicon glyphicon-plus btn-block';
														callback: [ self borrowUI: each bookNumber ] ] ].
												].
									html div
										class: 'col-sm-1';
										with: [ html
												form: [ html button
														class: 'btn btn-primary glyphicon glyphicon-search btn-block';
														callback: [ ObjectDetailSupport instance book: each ] ] ].
									currentUser isAdmin
												ifTrue: [ html div
														class: 'col-sm-1';
														with: [ html
																form: [ html button
																		class: 'glyphicon glyphicon-remove btn btn-danger btn-block';
																		callback: [ self removeUI: each bookNumber ] ] ] ].
									html break.
									html break.
									html break ] ] ] ]