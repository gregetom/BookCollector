rendering
renderDetail: html
	| bookDetail |
	bookDetail := ObjectDetailSupport instance book.
	bookDetail
		ifNotNil: [ html div
				class: 'container';
				style:
					'position: fixed;z-index: 1;padding-top: 100px;left: 0;top: 0;width: 100%; height: 100%; overflow:auto; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4)';
				with: [ html div
						class: 'container-fluid';
						with: [ html div
								class: 'col-sm-6 col-sm-offset-3';
								style: 'height: 40rem;background-color:white';
								with: [ html div
										class: 'col-sm-12 text-center';
										with: [ html heading
												level: 2;
												with: [ html strong: bookDetail title ].
											html break ].
									html div
										class: 'col-sm-12';
										with: [ html div
												class: 'col-sm-6';
												with: [ html image
														style: 'width: 100%; height:24rem';
														url: 'http://books.pharo.org/pfte.jpg' ].
											html div
												class: 'col-sm-6 well';
												with: [ html heading
														class: 'text-center';
														level: 4;
														with: [ html strong: 'Basic info' ].
													html break.
													html text: 'Number: '.
													html strong: bookDetail bookNumber.
													html break.
													html break.
													html text: 'Author: '.
													html strong: bookDetail author.
													html break.
													html break.
													html text: 'Genre: '.
													html strong: (bookDetail genre ifNotNil: [ bookDetail genre ] ifNil: [ '---' ]).
													html break.
													html break.
													html text: 'Publisher: '.
													html strong: (bookDetail publisher ifNotNil: [ bookDetail publisher name ] ifNil: [ '---' ]) ] ].
									html div
										class: 'col-sm-2';
										style: 'position: absolute;bottom: 0; margin: 2rem';
										with: [ html
												form: [ html button
														class: 'btn btn-primary btn-block';
														callback: [ ObjectDetailSupport instance book: nil ];
														with: [ html text: 'Back' ] ] ] ] ] ] ]