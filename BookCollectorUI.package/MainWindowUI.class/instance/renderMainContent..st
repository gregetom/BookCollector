rendering
renderMainContent: html
	html div
		class: 'container';
		style: 'padding-bottom:10rem';
		with: [ html div
				class: 'container-fluid text-center';
				with: [ html div
						class: 'col-sm-12';
						with: [ html div
								class: 'col-sm-3';
								with: [ html span
										class: 'glyphicon glyphicon-book';
										style: 'color: #f4511e;font-size: 200px';
										with: [ '' ] ].
							html div
								class: 'col-sm-9';
								style: 'text-align:left';
								with: [ html heading
										level: 1;
										with: [ html strong: 'Semestral project for BI-OOP' ] ] ] ] ]