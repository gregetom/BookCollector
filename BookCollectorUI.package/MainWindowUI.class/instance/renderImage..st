rendering
renderImage: html
	html div
		style: 'top:15rem;position: absolute;color: white;width:100%';
		with: [ html div
				class: 'text-center col-sm-12';
				with: [ html heading
						level: 1;
						style: 'font-size: 75px; color: white;background: rgba(0, 0, 0, 0.6);';
						with: [ html strong: 'Book collector' ] ] ].
	html image
		style: 'width: 100%; height:35rem;margin-bottom:5rem';
		url: 'http://www.readitforward.com/wp-content/uploads/2015/06/bookshelf-organization-1024x361.jpg'