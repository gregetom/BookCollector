rendering
renderHeader: html
	| currentUser |
	currentUser := UserService instance currentUser.
	html div
		class: 'navbar navbar-inverse';
		style: 'margin-bottom:0;border-radius: 0;position:fixed;width:100%;z-index:5000';
		with: [ html div
				class: 'container-fluid';
				with: [ html unorderedList
						class: 'nav navbar-nav';
						with: [ html
								listItem: [ html anchor
										url: '/bookCollector';
										with: 'Home' ].
							html
								listItem: [ html anchor
										url: '/books';
										with: 'Books' ].
							currentUser isAdmin
								ifTrue: [ html
										listItem: [ html anchor
												url: '/createBook';
												with: 'Create book' ].
									html
										listItem: [ html anchor
												url: '/publishers';
												with: 'Publishers' ].
									html
										listItem: [ html anchor
												url: '/users';
												with: 'Users' ] ] ].
					html unorderedList
						class: 'nav navbar-nav pull-right';
						with: [ currentUser isNil
								ifTrue: [ html
										listItem: [ html anchor
												url: 'login';
												with: 'Login' ] ].
							currentUser isNotNil
								ifTrue: [ currentUser isAdmin
										ifTrue: [ html
												listItem: [ html anchor
														callback: [ DatabaseSupport load ];
														with: 'Load from file' ].
											html
												listItem: [ html anchor
														callback: [ self hardReset ];
														with: 'Reset' ] ].
									html
										listItem: [ html anchor
												url: 'user';
												with: 'Logged as: ' , currentUser name , ' ' , currentUser surname ].
									html
										listItem: [ html anchor
												callback: [ self logoutUI ];
												with: 'Logout' ] ] ] ] ]