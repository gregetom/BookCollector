rendering
renderRegisterContent: html
	html div
		class: 'col-sm-8';
		with: [ html div
				class: 'well';
				with: [ html heading
						level: 2;
						with: [ html strong: 'Are you here first time?' ].
					html anchor
						class: 'btn btn-primary btn-block';
						url: '/register';
						with: 'Register' ] ]