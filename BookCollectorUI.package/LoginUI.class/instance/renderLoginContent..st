rendering
renderLoginContent: html
	| newUser |
	newUser := User new.
	html div
		class: 'col-sm-8';
		with: [ html div
				class: 'well';
				with: [ html heading
						level: 2;
						with: [ html strong: 'Please log in' ].
					loginErrorContainer successMessage
						ifNotNil: [ html div
								class: 'alert alert-success';
								with: loginErrorContainer successMessage ].
					loginErrorContainer failureMessage
						ifNotNil: [ html div
								class: 'alert alert-danger';
								with: loginErrorContainer failureMessage ].
					html
						form: [ html label: 'Email'.
							html textInput
								class: 'form-control';
								callback: [ :value | newUser email: value ].
							html break.
							html label: 'Password'.
							html passwordInput 
								class: 'form-control';
								callback: [ :value | newUser password: value ].
							html break.
							html button
								class: 'btn btn-primary btn-block center';
								callback: [ self loginUI: newUser ];
								with: 'Login' ] ] ]